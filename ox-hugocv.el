;;; ox-hugocv.el --- LaTeX hugocv Back-End for Org Export Engine -*- lexical-binding: t; -*-

;; Copyright (C) 2018 Free Software Foundation, Inc.

;; Author: Oscar Najera <hi AT oscarnajera.com DOT com>
;; Keywords: org, wp, tex

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;;
;; This library implements a LaTeX hugocv back-end, derived from the
;; LaTeX one.

;;; Code:
(require 'ox-hugo)
(require 'org-cv-utils)

;;; User-Configurable Variables

(defgroup org-export-hugocv nil
  "Options for exporting Org mode files to Hugo-compatible Markdown"
  :tag "Org Export Hugo CV"
  :group 'org-export
  :version "25.3")

;;; Define Back-End
(org-export-define-derived-backend 'hugocv 'hugo
  :options-alist
  '((:title "TITLE" nil nil parse)
    (:mobile "MOBILE" nil nil parse)
    (:homepage "HOMEPAGE" nil nil parse)
    (:address "ADDRESS" nil nil newline)
    (:city "CITY" nil nil newline)
    (:photo "PHOTO" nil nil parse)
    (:gitlab "GITLAB" nil nil parse)
    (:github "GITHUB" nil nil parse)
    (:linkedin "LINKEDIN" nil nil parse)
   ; (:with-email nil "email" t t)
    )
  :translate-alist '((headline . org-hugocv-headline)
                     (inner-template . org-hugocv-inner-template)))


(defun org-hugocv-org-timestamp-to-shortdate (date_str)
"Format orgmode timestamp DATE_STR  into a short form date.
Other strings are just returned unmodified

e.g. <2002-08-12 Mon> => Aug 2012
today => today"
  (if (string-match (org-re-timestamp 'active) date_str)
      (let* ((abbreviate 't)
             (dte (org-parse-time-string date_str))
             (day (nth 3 dte))
             (month (nth 4 dte))
             (year (nth 5 dte))) ;;'(02 07 2015)))
        (concat
         (number-to-string year)
         "-"
         (if (< month 10)
             (concat "0" (number-to-string month))
           (number-to-string month))
         "-"
         (if (< day 10)
             (concat "0" (number-to-string day))
         (number-to-string day))))
  date_str))



(defun org-hugocv--format-cventry (headline contents info)
  "Format HEADLINE as as cventry.
CONTENTS holds the contents of the headline.  INFO is a plist used
as a communication channel."
  (let* ((entry (org-cv-utils--parse-cventry headline info))
         (loffset (string-to-number (plist-get info :hugo-level-offset))) ;"" -> 0, "0" -> 0, "1" -> 1, ..
         (level (org-export-get-relative-level headline info)))
         ;(title (concat (make-string (+ loffset level)) " " (alist-get 'title entry))))
    (format "\n
{{<cventry title=\"%s\" start=\"%s\" end=\"%s\" employer=\"%s\" location=\"%s\">}}
%s

{{</cventry>}}
"
    (alist-get 'title entry)
    (org-hugocv-org-timestamp-to-shortdate (alist-get 'from-date entry))
    (if (alist-get 'to-date entry)
        (org-hugocv-org-timestamp-to-shortdate (alist-get 'to-date entry)) "")
    (alist-get 'employer entry)
    (alist-get 'location entry)
contents)))

(defun org-hugocv--format-cvactivity (headline contents info)
  "Format HEADLINE as as cventry.
CONTENTS holds the contents of the headline.  INFO is a plist used
as a communication channel."
  (let* ((entry (org-cv-utils--parse-cventry headline info))
         (loffset (string-to-number (plist-get info :hugo-level-offset))) ;"" -> 0, "0" -> 0, "1" -> 1, ..
         (level (org-export-get-relative-level headline info)))
         ;(title (concat (make-string (+ loffset level)) " " (alist-get 'title entry))))
    (format "\n
{{<cventry title=\"%s\" date=\"%s\" employer=\"%s\" location=\"%s\">}}
%s
{{</cventry>}}"
    (alist-get 'title entry)
    (alist-get 'from-date entry)
    (alist-get 'employer entry)
    (alist-get 'location entry)
contents)))




;;;; Headline
(defun org-hugocv-headline (headline contents info)
  "Transcode HEADLINE element into hugocv code.
CONTENTS is the contents of the headline.  INFO is a plist used
as a communication channel."
  (unless (org-element-property :footnote-section-p headline)
    (let ((environment (let ((env (org-element-property :CV_ENV headline)))
                         (or (org-string-nw-p env) "block"))))
      (cond
       ;; is a cv entry
       ((equal environment "cventry")
        (org-hugocv--format-cventry headline contents info))

       ((equal environment "cvactivity")
        (org-hugocv--format-cvactivity headline contents info))
       ((org-export-with-backend 'hugo headline contents info))))))

(defun org-hugocv-inner-template (contents info)
  "Return body of document after converting it to Hugo-compatible Markdown.
CONTENTS is the transcoded contents string.  INFO is a plist
holding export options."
  (concat "<div class=\"flex justify-between mb-1.5\">\n"
          (let((city (and (plist-get info :city)
                            (org-export-data (plist-get info :city) info))))
            (when (org-string-nw-p city)
              (format "<li class=\"list-none\"><i class=\"fas fa-home\"></i>&nbsp;&nbsp;%s</li>\n" city)))

          ;; phone
          (let((mobile (and (plist-get info :mobile)
                            (org-export-data (plist-get info :mobile) info))))
            (when (org-string-nw-p mobile)
              (format "<li class=\"list-none\"><i class=\"fas fa-mobile-alt\"></i>&nbsp;&nbsp;%s</li>\n" mobile)))

          (let((email (and (plist-get info :email)
                           (org-export-data (plist-get info :email) info))))

            (when (org-string-nw-p email)
              (format "<li class=\"list-none \">
        <a href=\"mailto:%s\" class=\"!no-underline !text-slate-700 dark:!text-slate-300 hover:!text-royal-600 dark:hover:!text-royal-400\"><span class=\"mr-3\"><i class=\"fas fa-envelope\"></i>&nbsp;&nbsp;%s</span></li></a>\n" email email)))

          "</div>\n\n"
                        (org-hugo-inner-template contents info)))


(provide 'ox-hugocv)
;;; ox-hugocv ends here
