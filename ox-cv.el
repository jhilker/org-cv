;;; ox-cv.el --- My Publishing Setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Jacob Hilker
;;
;; Author: Jacob Hilker <jacob.hilker2@gmail.com>
;; Maintainer: Jacob Hilker <jacob.hilker2@gmail.com>
;; Created: April 03, 2022
;; Modified: April 03, 2022
;; Version: 0.0.1
;; Keywords: abbrev bib c calendar comm convenience data docs emulations extensions faces files frames games hardware help hypermedia i18n internal languages lisp local maint mail matching mouse multimedia news outlines processes terminals tex tools unix vc wp
;; Homepage: https://github.com/jhilker1/ox-cv
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:
(require 'org-cv-utils)
(require 'ox-hugocv)
(require 'ox-moderncv)


(provide 'ox-cv)
;;; ox-cv.el ends here
